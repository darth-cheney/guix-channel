;; Guix Package for Guile NWS tools
(define-module (guile-nws)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix build-system guile)
  #:use-module (guix git-download)
  ;#:use-module (gnu packages guile guile-json)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages tls))

(define-public guile-nws
  (let ((commit "f029634a20ccecd80e340620d00d6a04ef72c002"))
    (package
     (name "guile-nws")
     (version "0.1a")
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/darth-cheney/guile-nws.git")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0awmaidx7gz8q4smrwv4h5s6739cxk11nqibfnybk8fcvrfx2nqm"))
              ))
     (build-system guile-build-system)
     (arguments
      '(#:phases (modify-phases %standard-phases
                                (add-after 'build 'copy-scripts
                                           (lambda* (#:key outputs #:allow-other-keys)
                                             (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
                                               (install-file "scripts/iptogeo" bin)
                                               (install-file "scripts/myip" bin) #t)
                                             )))))
     (propagated-inputs
      `(("guile" ,guile-3.0)
        ("guile-json" ,guile-json-4)
        ("gnutls" ,gnutls)))
     (synopsis "National Weather Service (NWS) API Tools for Guile Scheme")
     (description "National Weather Service (NWS) API Tools for Guile Scheme")
     (home-page "https://gitlab.com/darth-cheney/guile-nws")
     (license gpl2))))

guile-nws
